import 'package:http/http.dart';
import 'dart:convert';
import 'package:intl/intl.dart';

class WorldTime {
  String location;
  String flag;
  String time;
  String url;
  bool isDaytime;

  WorldTime({this.location, this.flag, this.url});

  Future<void> getTime() async {
    try {
      Response response =
          await get(Uri.parse('http://worldtimeapi.org/api/timezone/$url'));
      Map data = jsonDecode(response.body);
      //print(data);
      String dateTime = data['datetime'];
      String offset = data['utc_offset'].substring(0, 3);
      //print(dateTime);
      //print(offset);

      DateTime now = DateTime.parse(dateTime.substring(0, 26));
      //now = now.add(Duration(hours: int.parse(offset)));

      //print(now);
      isDaytime = now.hour > 6 && now.hour < 20 ? true : false;
      time = DateFormat.jm().format(now);
    } catch (e) {
      print('Caught error: $e');
      time = 'Could not get data';
    }
  }
}
